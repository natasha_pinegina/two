﻿
// DESDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "DES.h"
#include "DESDlg.h"
#include "afxdialogex.h"
#include <iostream>
#include <string>
#include <fstream>
#include <bitset>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
using namespace std;

// Диалоговое окно CDESDlg



CDESDlg::CDESDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DES_DIALOG, pParent)
	, InitText(_T(""))
	, ShifrText(_T(""))
	, Key(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDESDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_InitText, InitText);
	DDX_Text(pDX, IDC_ShifrText, ShifrText);
	DDX_Text(pDX, IDC_Key, Key);
	DDX_Control(pDX, IDC_Shifr, Shifr);
	DDX_Control(pDX, IDC_DeShifr, DeShifr);
}

BEGIN_MESSAGE_MAP(CDESDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_GetText, &CDESDlg::OnBnClickedGettext)
	ON_BN_CLICKED(IDC_GetKey, &CDESDlg::OnBnClickedGetkey)
	ON_BN_CLICKED(IDC_Do, &CDESDlg::OnBnClickedDo)
END_MESSAGE_MAP()


// Обработчики сообщений CDESDlg

BOOL CDESDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CDESDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CDESDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CDESDlg::OnBnClickedGettext()
{
	CFileDialog fileDialog(TRUE, NULL, L"*.txt");
	int res = fileDialog.DoModal();
	if (res != IDOK)
		return;
	CFile file;
	file.Open(fileDialog.GetPathName(), CFile::modeRead);
	CStringA str;
	LPSTR pBuf = str.GetBuffer(file.GetLength() + 1);
	file.Read(pBuf, file.GetLength() + 1);
	pBuf[file.GetLength()] = NULL;
	//CStringA decodedText = str;
	InitText = str;
	file.Close();
	str.ReleaseBuffer();
	UpdateData(FALSE);
}

int randomRange(int low, int high)
{
	return rand() % (high - low + 1) + low;
}

void CDESDlg::OnBnClickedGetkey()
{
	char* newKey = new char[7];
	memset(newKey, 0, 7 * sizeof(char));
	Key = "";
	srand(time(NULL));
	for (int n = 0; n < 8; n++)
	{
		newKey[n] = randomRange('a', 'z');
		Key += newKey[n];
	}
	//string str(newKey);
	UpdateData(false);
}

static int ip[64]{ 57, 49, 41, 33, 25, 17, 9, 1,
										59, 51, 43, 35, 27, 19, 11, 3,
										61, 53, 45, 37, 29, 21, 13, 5,
										63, 55, 47, 39, 31, 23, 15, 7,
										56, 48, 40, 32, 24, 16,  8, 0,
										58, 50, 42, 34, 26, 18, 10, 2,
										60, 52, 44, 36, 28, 20, 12, 4,
										62, 54, 46, 38, 30, 22, 14, 6 };

static int expansion[48]{ 31,  0,  1,  2,  3,  4,  3,  4,
										 5,  6,  7,  8,  7,  8, 9, 10,
										11, 12, 11, 12, 13, 14, 15, 16,
										15, 16, 17, 18, 19, 20, 19, 20,
										21, 22, 23, 24, 23, 24, 25, 26,
										27, 28, 27, 28, 29, 30, 31,  0 };

static byte s_block[8][4][16]
{
					   {{0x0e, 0x04, 0x0d, 0x01, 0x02, 0x0f, 0x0b, 0x08, 0x03, 0x0a, 0x06, 0x0c, 0x05, 0x09, 0x00, 0x07},
						{0x00, 0x0f, 0x07, 0x04, 0x0e, 0x02, 0x0d, 0x01, 0x0a, 0x06, 0x0c, 0x0b, 0x09, 0x05, 0x03, 0x08},
						{0x04, 0x01, 0x04, 0x08, 0x0d, 0x06, 0x02, 0x0b, 0x0f, 0x0c, 0x09, 0x07, 0x03, 0x0a, 0x05, 0x00},
						{0x0f, 0x0c, 0x08, 0x02, 0x04, 0x09, 0x01, 0x07, 0x05, 0x0b, 0x03, 0x0e, 0x0a, 0x00, 0x06, 0x0d}},
					   {{0x0f, 0x01, 0x08, 0x0e, 0x06, 0x0b, 0x03, 0x04, 0x09, 0x07, 0x02, 0x0d, 0x0c, 0x00, 0x05, 0x0a},
						{0x03, 0x0d, 0x04, 0x07, 0x0f, 0x02, 0x08, 0x0e, 0x0c, 0x00, 0x01, 0x0a, 0x06, 0x09, 0x0b, 0x05},
						{0x00, 0x0e, 0x07, 0x0b, 0x0a, 0x04, 0x0d, 0x01, 0x05, 0x08, 0x0c, 0x06, 0x09, 0x03, 0x02, 0x0f},
						{0x0d, 0x08, 0x0a, 0x01, 0x03, 0x0f, 0x04, 0x02, 0x0b, 0x06, 0x07, 0x0c, 0x00, 0x05, 0x0e, 0x09}},
					   {{0x0a, 0x00, 0x09, 0x0e, 0x06, 0x03, 0x0f, 0x05, 0x01, 0x0d, 0x0c, 0x07, 0x0b, 0x04, 0x02, 0x08},
						{0x0d, 0x07, 0x00, 0x09, 0x03, 0x04, 0x06, 0x0a, 0x02, 0x08, 0x05, 0x0e, 0x0c, 0x0b, 0x0f, 0x01},
						{0x0d, 0x06, 0x04, 0x09, 0x08, 0x0f, 0x03, 0x00, 0x0b, 0x01, 0x02, 0x0c, 0x05, 0x0a, 0x0e, 0x07},
						{0x01, 0x0a, 0x0d, 0x00, 0x06, 0x09, 0x08, 0x07, 0x04, 0x0f, 0x0e, 0x03, 0x0b, 0x05, 0x02, 0x0c}},
					   {{0x07, 0x0d, 0x0e, 0x03, 0x00, 0x06, 0x09, 0x0a, 0x01, 0x02, 0x08, 0x05, 0x0b, 0x0c, 0x04, 0x0f},
						{0x0d, 0x08, 0x0b, 0x05, 0x06, 0x0f, 0x00, 0x03, 0x04, 0x07, 0x02, 0x0c, 0x01, 0x0a, 0x0e, 0x09},
						{0x0a, 0x06, 0x09, 0x00, 0x0c, 0x0b, 0x07, 0x0d, 0x0f, 0x01, 0x03, 0x0e, 0x05, 0x02, 0x08, 0x04},
						{0x03, 0x0f, 0x00, 0x06, 0x0a, 0x01, 0x0d, 0x08, 0x09, 0x04, 0x05, 0x0b, 0x0c, 0x07, 0x02, 0x0e}},
					   {{0x02, 0x0c, 0x04, 0x01, 0x07, 0x0a, 0x0b, 0x06, 0x08, 0x05, 0x03, 0x0f, 0x0d, 0x00, 0x0e, 0x09},
						{0x0e, 0x0b, 0x02, 0x0c, 0x04, 0x07, 0x0d, 0x01, 0x05, 0x00, 0x0f, 0x0a, 0x03, 0x09, 0x08, 0x06},
						{0x04, 0x02, 0x01, 0x0b, 0x0a, 0x0d, 0x07, 0x08, 0x0f, 0x09, 0x0c, 0x05, 0x06, 0x03, 0x00, 0x0e},
						{0x0b, 0x08, 0x0c, 0x07, 0x01, 0x0e, 0x02, 0x0d, 0x06, 0x0f, 0x00, 0x09, 0x0a, 0x04, 0x05, 0x03}},
					   {{0x0c, 0x01, 0x0a, 0x0f, 0x09, 0x02, 0x06, 0x08, 0x00, 0x0d, 0x03, 0x04, 0x0e, 0x07, 0x05, 0x0b},
						{0x0a, 0x0f, 0x04, 0x02, 0x07, 0x0c, 0x09, 0x05, 0x06, 0x01, 0x0d, 0x0e, 0x00, 0x0b, 0x03, 0x08},
						{0x09, 0x0e, 0x0f, 0x05, 0x02, 0x08, 0x0c, 0x03, 0x07, 0x00, 0x04, 0x0a, 0x01, 0x0d, 0x01, 0x06},
						{0x04, 0x03, 0x02, 0x0c, 0x09, 0x05, 0x0f, 0x0a, 0x0b, 0x0e, 0x01, 0x07, 0x06, 0x00, 0x08, 0x0d}},
					   {{0x04, 0x0b, 0x02, 0x0e, 0x0f, 0x00, 0x08, 0x0d, 0x03, 0x0c, 0x09, 0x07, 0x05, 0x0a, 0x06, 0x01},
						{0x0d, 0x00, 0x0b, 0x07, 0x04, 0x09, 0x01, 0x0a, 0x0e, 0x03, 0x05, 0x0c, 0x02, 0x0f, 0x08, 0x06},
						{0x01, 0x04, 0x0b, 0x0d, 0x0c, 0x03, 0x07, 0x0e, 0x0a, 0x0f, 0x06, 0x08, 0x00, 0x05, 0x09, 0x02},
						{0x06, 0x0b, 0x0d, 0x08, 0x01, 0x04, 0x0a, 0x07, 0x09, 0x05, 0x00, 0x0f, 0x0e, 0x02, 0x03, 0x0c}},
					   {{0x0d, 0x02, 0x08, 0x04, 0x06, 0x0f, 0x0b, 0x01, 0x0a, 0x09, 0x03, 0x0e, 0x05, 0x00, 0x0c, 0x07},
						{0x01, 0x0f, 0x0d, 0x08, 0x0a, 0x03, 0x07, 0x04, 0x0c, 0x05, 0x06, 0x0b, 0x00, 0x0e, 0x09, 0x02},
						{0x07, 0x0b, 0x04, 0x01, 0x09, 0x0c, 0x0e, 0x02, 0x00, 0x06, 0x0a, 0x0d, 0x0f, 0x03, 0x05, 0x08},
						{0x02, 0x01, 0x0e, 0x07, 0x04, 0x0a, 0x08, 0x0d, 0x0f, 0x0c, 0x09, 0x00, 0x03, 0x05, 0x06, 0x0b}}
};

static int permutation_func[32]{ 15,  6, 19, 20, 28, 11, 27, 16,
												 0, 14, 22, 25,  4, 18, 30, 9,
												 1,  7, 23, 13, 31, 26,  2, 8,
												18, 12, 29,  5, 21, 10,  3, 24 };

static int final_permutation[64]{ 39, 7, 47, 15, 55, 23, 63, 31,
												38, 6, 46, 14, 54, 22, 62, 30,
												37, 5, 45, 13, 53, 21, 61, 29,
												36, 4, 44, 12, 52, 20, 60, 28,
												35, 3, 43, 11, 51, 19, 59, 27,
												34, 2, 42, 10, 50, 18, 58, 26,
												33, 1, 41,  9, 49, 17, 57, 25,
												32, 0, 40,  8, 48, 16, 56, 24 };

static int C_block[28]{ 56, 48, 40, 32, 24, 16, 8,
													0, 57, 49, 41, 33, 25, 17,
													9, 1, 58, 50, 42, 34, 26,
													18, 10, 2, 59, 51, 43, 35 };

static int D_block[28]{ 62, 54, 46, 38, 30, 22, 14,
													6, 61, 53, 45, 37, 29, 21,
													13, 5, 60, 52, 44, 36, 28,
													20, 12, 4, 27, 19, 11, 3 };

static int key_block[48]{ 13, 16, 10, 23, 0, 4,
											2, 27, 14, 5,  20,  9,
											22, 18, 11, 3, 25, 7,
											15, 6, 26, 19, 12, 1,
											40, 51, 30, 36, 46, 54,
											29, 39, 50, 44, 32, 47,
											43, 48, 38, 55, 33, 52,
											45, 41, 49, 35, 28, 31 };

static int key_shift[16]{ 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };


string help_text;

string IP_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 64; i++)
	{
		res += s[ip[i]];
	}
	return res;
}

string E_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 48; i++)
	{
		res += s[expansion[i]];
	}
	return res;
}

string C_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 28; i++)
	{
		res += s[C_block[i]];
	}
	return res;
}

string D_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 28; i++)
	{
		res += s[D_block[i]];
	}
	return res;
}

string CD_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 48; i++)
	{
		res += s[key_block[i]];
	}
	return res;
}

string P_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 32; i++)
	{
		res += s[permutation_func[i]];
	}
	return res;
}

string IP_MINUS_METHOD(string s)
{
	string res = "";
	for (int i = 0; i < 64; i++)
	{
		res += s[final_permutation[i]];
	}
	return res;
}

string XORf(string s1, string s2)
{
	string result = "";

	for (int i = 0; i < s1.length(); i++)
	{
		if (s1[i] == s2[i])
		{
			result += '0';
		}
		else
		{
			result += '1';
		}
	}
	return result;
}

string f(string R, string k)
{
	string expanshion_r = E_METHOD(R);
	string pred_s_block = XORf(expanshion_r, k);
	string pred_s_block_to_block[8];

	for (int i = 0; i < 8; i++)
	{
		pred_s_block_to_block[i] = pred_s_block.substr(i * 6, 6);
	}

	string NumberOfString[8];
	string NumberOfColumn[8];
	int NumberOfStringHex[8]{ 0 };
	int NumberOfColumnHex[8]{ 0 };
	int S_block_number_int[8]{ 0 };
	string S_block_number_str[8];
	string S_block_number_bin[8];
	string S_block_out = "";
	string S_block_out_reverse = "";

	for (int i = 0; i < 8; i++)
	{
		NumberOfString[i] += pred_s_block_to_block[i][0];
		NumberOfString[i] += pred_s_block_to_block[i][5];

		NumberOfColumn[i] += pred_s_block_to_block[i][1];
		NumberOfColumn[i] += pred_s_block_to_block[i][2];
		NumberOfColumn[i] += pred_s_block_to_block[i][3];
		NumberOfColumn[i] += pred_s_block_to_block[i][4];

		for (int j = 0; j < NumberOfString[i].length(); j++)
		{
			NumberOfStringHex[i] *= 2;
			NumberOfStringHex[i] += NumberOfString[i][j] - '0';
		}

		for (int j = 0; j < NumberOfColumn[i].length(); j++)
		{
			NumberOfColumnHex[i] *= 2;
			NumberOfColumnHex[i] += NumberOfColumn[i][j] - '0';
		}

		S_block_number_int[i] = s_block[i][NumberOfStringHex[i]][NumberOfColumnHex[i]];
		S_block_number_str[i] = S_block_number_int[i];


		for (int c : S_block_number_str[i])
		{
			bitset<4> bs(c);
			S_block_number_bin[i] += bs.to_string();
			S_block_out += S_block_number_bin[i];
		}
	}

	S_block_out_reverse = P_METHOD(S_block_out);

	return S_block_out_reverse;
}


string BinaryToNormal(string s)
{
	string output = "";
	string* Blocks = new string[s.length() / 8];
	for (int i = 0; i < s.length() / 8; i++)
	{
		Blocks[i] = s.substr(i * 8, 8);
	}
	for (int i = 0; i < s.length() / 8; i++)
	{
		bitset<8> str(Blocks[i]);
		unsigned char ch = static_cast <unsigned char> (str.to_ulong());
		output += ch;
	}
	delete[] Blocks;
	return output;
}
void CDESDlg::DES(string* s,string k)
{
//начальная перестановка
string StartRevers = IP_METHOD(s[0]);		
string L = s[0].substr(0, LengthOfBlock / 2);		
string R = s[0].substr(LengthOfBlock / 2, LengthOfBlock / 2);			
string res = XORf(L, R);

//расширяем R
string expanshion_r = E_METHOD(R);

//значение ключа 56 бит
string key56 = "";
for (int i = 0; i < k.length(); i++)
{
	if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63)
	{
		key56 += k[i];
	}
}

string C = key56.substr(0, key56.length() / 2);
string D = key56.substr(key56.length() / 2, key56.length() / 2);

//перестановка C, D блоков

string C_reverse[17];
string D_reverse[17];
string C0_reverse = C_METHOD(k);
string D0_reverse = D_METHOD(k);
string CD0 = C0_reverse + D0_reverse;

C_reverse[0] = C0_reverse;
D_reverse[0] = D0_reverse;
string buf1;
string buf2;

for (int i = 1; i <= QuantityOfRounds; i++)
{
	if ((i == 1) || (i == 2) || (i == 9) || (i == 16))
	{
		buf1 = "";
		buf1 = C_reverse[i - 1][0];
		C_reverse[i] = C_reverse[i - 1] + buf1;
		C_reverse[i] = C_reverse[i].erase(0, 1);

		buf2 = "";
		buf2 = D_reverse[i - 1][0];
		D_reverse[i] = D_reverse[i - 1] + buf2;
		D_reverse[i] = D_reverse[i].erase(0, 1);
	}
	else
	{
		buf1 = "";
		buf1 += C_reverse[i - 1][0];
		buf1 += C_reverse[i - 1][1];
		C_reverse[i] = C_reverse[i - 1] + buf1;
		C_reverse[i] = C_reverse[i].erase(0, 2);

		buf2 = "";
		buf2 += D_reverse[i - 1][0];
		buf2 += D_reverse[i - 1][1];
		D_reverse[i] = D_reverse[i - 1] + buf2;
		D_reverse[i] = D_reverse[i].erase(0, 2);
	}
}

string CD[16];
string key48[16];
for (int j = 0; j < QuantityOfRounds; j++)
{
	CD[j] = C_reverse[j + 1] + D_reverse[j + 1];
	key48[j] = CD_METHOD(CD[j]);
}

string pred_s_block = XORf(expanshion_r, key48[0]);
string pred_s_block_to_block[8];

for (int i = 0; i < 8; i++)
{
	pred_s_block_to_block[i] = pred_s_block.substr(i * 6, 6);
}

string NumberOfString[8];
string NumberOfColumn[8];
int NumberOfStringHex[8]{ 0 };
int NumberOfColumnHex[8]{ 0 };
int S_block_number_int[8]{ 0 };
string S_block_number_str[8];
string S_block_number_bin[8];
string S_block_out = "";

for (int i = 0; i < 8; i++)
{
	NumberOfString[i] += pred_s_block_to_block[i][0];
	NumberOfString[i] += pred_s_block_to_block[i][5];

	NumberOfColumn[i] += pred_s_block_to_block[i][1];
	NumberOfColumn[i] += pred_s_block_to_block[i][2];
	NumberOfColumn[i] += pred_s_block_to_block[i][3];
	NumberOfColumn[i] += pred_s_block_to_block[i][4];

	for (int j = 0; j < NumberOfString[i].length(); j++)
	{
		NumberOfStringHex[i] *= 2;
		NumberOfStringHex[i] += NumberOfString[i][j] - '0';
	}

	for (int j = 0; j < NumberOfColumn[i].length(); j++)
	{
		NumberOfColumnHex[i] *= 2;
		NumberOfColumnHex[i] += NumberOfColumn[i][j] - '0';
	}

	S_block_number_int[i] = s_block[i][NumberOfStringHex[i]][NumberOfColumnHex[i]];
	S_block_number_str[i] = S_block_number_int[i];


	for (int c : S_block_number_str[i])
	{
		bitset<4> bs(c);
		S_block_number_bin[i] += bs.to_string();
		S_block_out += S_block_number_bin[i];
	}
}


string S_block_out_reverse = P_METHOD(S_block_out);

//алгоритм Фейстеля

string StartReversBlocks = "";
string L0 = "";
string R0 = "";
string L_part[16];
string R_part[16];
string LR = "";
string LR_revers;
string ResultOfFeistel = "";

//шифрование
for (int j = 0; j < NewSizeOfBlock; j++)
{

	StartReversBlocks = IP_METHOD(s[j]);
	L0 = StartReversBlocks.substr(0, LengthOfBlock / 2);
	R0 = StartReversBlocks.substr(LengthOfBlock / 2, LengthOfBlock / 2);

	for (int i = 0; i < QuantityOfRounds; i++)
	{
		if (i == 0)
		{
			L_part[i] = R0;
			R_part[i] = XORf(L0, f(R0, key48[i]));
		}
		else
		{
			L_part[i] = R_part[i - 1];
			R_part[i] = XORf(L_part[i - 1], f(R_part[i - 1], key48[i]));
		}
	}

	LR = "";
	LR_revers = "";
	LR = R_part[15] + L_part[15];
	LR_revers = IP_MINUS_METHOD(LR);

	ResultOfFeistel += LR_revers;
}

string exit = BinaryToNormal(ResultOfFeistel);
help_text = exit;
ShifrText = (CString)help_text.c_str();
}

void CDESDlg::obrDES(string* s, string k)
{
	//начальная перестановка
string StartRevers = IP_METHOD(s[0]);		

string L = s[0].substr(0, LengthOfBlock / 2);		
string R = s[0].substr(LengthOfBlock / 2, LengthOfBlock / 2);			
string res = XORf(L, R);

//расширяем R
string expanshion_r = E_METHOD(R);

//значение ключа 56 бит
string key56 = "";
for (int i = 0; i <k.length(); i++)
{
	if (i != 7 && i != 15 && i != 23 && i != 31 && i != 39 && i != 47 && i != 55 && i != 63)
	{
		key56 += k[i];
	}
}

string C = key56.substr(0, key56.length() / 2);
string D = key56.substr(key56.length() / 2, key56.length() / 2);

//перестановка C, D блоков

string C_reverse[17];
string D_reverse[17];
string C0_reverse = C_METHOD(k);
string D0_reverse = D_METHOD(k);
string CD0 = C0_reverse + D0_reverse;

C_reverse[0] = C0_reverse;
D_reverse[0] = D0_reverse;
string buf1;
string buf2;

for (int i = 1; i <= QuantityOfRounds; i++)
{
	if ((i == 1) || (i == 2) || (i == 9) || (i == 16))
	{
		buf1 = "";
		buf1 = C_reverse[i - 1][0];
		C_reverse[i] = C_reverse[i - 1] + buf1;
		C_reverse[i] = C_reverse[i].erase(0, 1);

		buf2 = "";
		buf2 = D_reverse[i - 1][0];
		D_reverse[i] = D_reverse[i - 1] + buf2;
		D_reverse[i] = D_reverse[i].erase(0, 1);
	}
	else
	{
		buf1 = "";
		buf1 += C_reverse[i - 1][0];
		buf1 += C_reverse[i - 1][1];
		C_reverse[i] = C_reverse[i - 1] + buf1;
		C_reverse[i] = C_reverse[i].erase(0, 2);

		buf2 = "";
		buf2 += D_reverse[i - 1][0];
		buf2 += D_reverse[i - 1][1];
		D_reverse[i] = D_reverse[i - 1] + buf2;
		D_reverse[i] = D_reverse[i].erase(0, 2);
	}
}

//ключ из 56 бит в 48 бит

string CD[16];
string key48[16];
for (int j = 0; j < QuantityOfRounds; j++)
{
	CD[j] = C_reverse[j + 1] + D_reverse[j + 1];
	key48[j] = CD_METHOD(CD[j]);
	
}

//нахождение функции f
string pred_s_block = XORf(expanshion_r, key48[0]);
string pred_s_block_to_block[8];

for (int i = 0; i < 8; i++)
{
	pred_s_block_to_block[i] = pred_s_block.substr(i * 6, 6);
}


string NumberOfString[8];
string NumberOfColumn[8];
int NumberOfStringHex[8]{ 0 };
int NumberOfColumnHex[8]{ 0 };
int S_block_number_int[8]{ 0 };
string S_block_number_str[8];
string S_block_number_bin[8];
string S_block_out = "";

for (int i = 0; i < 8; i++)
{
	NumberOfString[i] += pred_s_block_to_block[i][0];
	NumberOfString[i] += pred_s_block_to_block[i][5];

	NumberOfColumn[i] += pred_s_block_to_block[i][1];
	NumberOfColumn[i] += pred_s_block_to_block[i][2];
	NumberOfColumn[i] += pred_s_block_to_block[i][3];
	NumberOfColumn[i] += pred_s_block_to_block[i][4];

	for (int j = 0; j < NumberOfString[i].length(); j++)
	{
		NumberOfStringHex[i] *= 2;
		NumberOfStringHex[i] += NumberOfString[i][j] - '0';
	}

	for (int j = 0; j < NumberOfColumn[i].length(); j++)
	{
		NumberOfColumnHex[i] *= 2;
		NumberOfColumnHex[i] += NumberOfColumn[i][j] - '0';
	}

	S_block_number_int[i] = s_block[i][NumberOfStringHex[i]][NumberOfColumnHex[i]];
	S_block_number_str[i] = S_block_number_int[i];


	for (int c : S_block_number_str[i])
	{
		bitset<4> bs(c);
		S_block_number_bin[i] += bs.to_string();
		S_block_out += S_block_number_bin[i];
	}
}

string S_block_out_reverse = P_METHOD(S_block_out);

//алгоритм Фейстеля

string StartReversBlocks = "";
string L0 = "";
string R0 = "";
string L_part[16];
string R_part[16];
string LR = "";
string LR_revers;
string ResultOfFeistel = "";

//шифрование
for (int j = 0; j < NewSizeOfBlock; j++)
{

	StartReversBlocks = IP_METHOD(s[j]);
	L0 = StartReversBlocks.substr(0, LengthOfBlock / 2);
	R0 = StartReversBlocks.substr(LengthOfBlock / 2, LengthOfBlock / 2);

	
	for (int i = 0; i < QuantityOfRounds; i++)
	{
		if (i == 0)
		{
			L_part[i] = R0;
			R_part[i] = XORf(L0, f(R0, key48[QuantityOfRounds - 1 - i]));
		}
		else
		{
			L_part[i] = R_part[i - 1];
			R_part[i] = XORf(L_part[i - 1], f(R_part[i - 1], key48[QuantityOfRounds - 1 - i]));
		}
	}

	LR = "";
	LR_revers = "";
	LR = R_part[15] + L_part[15];
	LR_revers = IP_MINUS_METHOD(LR);

	ResultOfFeistel += LR_revers;
}

string exit = BinaryToNormal(ResultOfFeistel);


help_text = exit;
InitText = help_text.c_str();
}


void CDESDlg::OnBnClickedDo()
{
	UpdateData(TRUE);
	if (Shifr.GetCheck())
	{
		ShifrText = " ";
		string res;
		CString Text = InitText;
		CString Ckey = Key;
		string data;
		data.resize(Text.GetLength());
		WideCharToMultiByte(CP_ACP, 0, Text, -1, &data[0], data.size(), NULL, NULL);

		string key;
		key.resize(Ckey.GetLength());
		WideCharToMultiByte(CP_ACP, 0, Ckey, -1, &key[0], key.size(), NULL, NULL);

		
		
		int sum, ostatok;
		string BinaryKey = "";

		for (char c : key)		
		{
			sum = 0;
			ostatok = 0;

			bitset<8> bs(c);		
			string bitkey56 = "";
			bitkey56 = bs.to_string();		

			for (int i = 0; i < bitkey56.length() - 1; i++)
			{
				sum += bitkey56[i];		
			}
			ostatok = sum % 2;		

			if (ostatok == 0)
			{
				if (bitkey56.back() != '1')	bitkey56.back() = '1';
			}
			BinaryKey += bitkey56;

		}

		int dif_before = data.length() % key.length();		
		if (dif_before != 0)
		{
			int additionLength = key.length() - dif_before;
			int NewLen = data.length() + additionLength;
			data.resize(NewLen, '0');
		}
		

		string BinaryData = "";
		for (char c : data)		
		{
			bitset<8> bs(c);
			BinaryData += bs.to_string();
		}

		NewSizeOfBlock = BinaryData.length() / SizeOfBlock;


		string* Blocks = new string[NewSizeOfBlock];		
		LengthOfBlock = 0;
		LengthOfBlock = BinaryData.length() / NewSizeOfBlock;
		res = "";
		for (int i = 0; i < NewSizeOfBlock; i++)
		{
			Blocks[i] = BinaryData.substr(i * LengthOfBlock, LengthOfBlock);		
		}
		DES(Blocks, BinaryKey);
	}
	if (DeShifr.GetCheck())
	{
		InitText = " ";	

		CString CKey = Key;
		
		string data = help_text;
		
		string keyhelp;
		keyhelp.resize(Key.GetLength());
		WideCharToMultiByte(CP_ACP, 0, Key, -1, &keyhelp[0], keyhelp.size(), NULL, NULL);

		int sum, ostatok;
		
		string BinaryKey = "";

		for (char c : keyhelp)		
		{
			sum = 0;
			ostatok = 0;

			bitset<8> bs(c);		
			string bitkey56 = "";
			bitkey56 = bs.to_string();		

			for (int i = 0; i < bitkey56.length() - 1; i++)
			{
				sum += bitkey56[i];		
			}
			ostatok = sum % 2;		

			if (ostatok == 0)
			{
				if (bitkey56.back() != '1')	bitkey56.back() = '1';
			}
			BinaryKey += bitkey56;
		}
		
		int dif_before = data.length() % keyhelp.length();		
		if (dif_before != 0)
		{
			int additionLength = keyhelp.length() - dif_before;
			int NewLen = data.length() + additionLength;
			data.resize(NewLen, '0');
		}

		
		string BinaryData = "";
		for (char c : data)		
		{
			bitset<8> bs(c);
			BinaryData += bs.to_string();
		}

		int NewSizeOfBlock = BinaryData.length() / SizeOfBlock;

		string* Blocks = new string[NewSizeOfBlock];		
		int LengthOfBlock = BinaryData.length() / NewSizeOfBlock;

		for (int i = 0; i < NewSizeOfBlock; i++)
		{
			Blocks[i] = BinaryData.substr(i * LengthOfBlock, LengthOfBlock);		
		}
		obrDES(Blocks, BinaryKey);
	}
	UpdateData(FALSE);
}
