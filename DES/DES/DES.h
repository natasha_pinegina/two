﻿
// DES.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CDESApp:
// Сведения о реализации этого класса: DES.cpp
//

class CDESApp : public CWinApp
{
public:
	CDESApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CDESApp theApp;
