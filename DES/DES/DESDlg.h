﻿
// DESDlg.h: файл заголовка
//

#pragma once
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

// Диалоговое окно CDESDlg
class CDESDlg : public CDialogEx
{
// Создание
public:
	CDESDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DES_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString InitText;
	CString ShifrText;
    string abracodabra;
    
   

    afx_msg void OnBnClickedGettext();
    CString Key;
    string kk;
    afx_msg void OnBnClickedGetkey();
    CButton Shifr;
    CButton DeShifr;
    afx_msg void OnBnClickedDo();
    afx_msg void DES(string* s, string k);
    afx_msg void obrDES(string* s, string k);
    int LengthOfBlock = 0;
    const int SizeOfBlock = 64;		//длина блока в двоич.
    const int SizeOfChar = 8;		//длина символа в двоич.
    const int QuantityOfRounds = 16;		//число итераций цикла фейстеля
    int NewSizeOfBlock = 0;
};
